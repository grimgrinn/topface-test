
var hashtags = [];
var tagsTodel = [];

window.onload = function(){
	console.log('ready');
	
	
	var rec = false;
	var hashtag = '';
	

	localStorage.getItem('last') ?  true : localStorage.setItem('last', 0);
	
	
	///////////////////////////////////////////////////////////////some vars
	var quantity 		  =	localStorage.getItem('last'); 
	var modals 			  =	document.querySelectorAll('.modal');
	var shortcuts 		  =	document.querySelectorAll('.bookmark-shortcut');
	var closeBtn 		  =	document.querySelectorAll('.close-btn');
	var editable 		  =	document.querySelectorAll('.editable');
	var bBody 			  =	document.querySelector('.modal-bookmark__bookmark-body');
	var newbBody 		  =	document.querySelector('.bookmark-body_creation');
	var shortcutsPlace    =	document.querySelector('.shortcuts');
	var modalBg 		  =	document.querySelector('.modal-bg');
	var editBtn 		  =	document.querySelector('.modal-bookmark__edi');
	var saveBtn 		  =	document.querySelector('.modal-bookmark__sav');
	var delBtn 			  =	document.querySelector('.modal-bookmark__del');
	var submitBookmarkBtn = document.querySelector('.modal-bookmark__cre');
	var addBokmark 		  =	document.querySelector('.modal-bookmark__bookmark-body');
	var newBookmark 	  =	document.querySelector('.modal-bookmark_creation');
	var oldBookmark 	  =	document.querySelector('.modal-bookmark_existed');
	var createBtn 		  =	document.querySelector('.add-bookmark');
	var tagDiv 			  = document.querySelectorAll('.tag'); 
	var tagDel 			  = document.querySelectorAll('.tag-delete'); 
	console.log('try to render');
	
	render(shortcutsPlace, false, false, n=>{ //render all bookamrks in the universe
		n.addEventListener('click',()=>{
			recall(oldBookmark, n.dataset.id);
			show([oldBookmark,modalBg]);
			hide(newBookmark);
			hashtags = [];
				
			},false);
	});

	///////////////////////////////////////////////////////////////close buttons
	for(var i = 0; i < closeBtn.length; i++){
		closeBtn[i].addEventListener('click',function(){
					var p = this.parentNode;			
					hide(p); 
					hasClass(p,'modal') ? hide(modals) : true;
 				},false);
	}

	///////////////////////////////////////////////////////////////edit bookmark
	editBtn.addEventListener('click',()=>{
		
		toggleContenteditable(editable);
		toggleClass(editable, 'editing');
		if(hasClass(saveBtn, 'disabled'))
			toggleClass(saveBtn, 'disabled');
			hashtags = [];
		}, false);

	
	///////////////////////////////////////////////////////////////create bookmark
	createBtn.addEventListener('click', ()=>{
		show([newBookmark,modalBg]);
	},false);


	///////////////////////////////////////////////////////////////listen keyboard for bookmarks
	for(var i = 0; i < editable.length; i++){
		editable[i].addEventListener('keydown', function(event){
			const keyName = event.key;
			const keyCode = event.code;
			var sym = getChar(event);
			if(sym){
			
					if(keyName == '#'){
						rec = true;
						hashtag = '';
					}
			  	
			if(keyCode == 'Space' && rec){
			  	rec = false;
			  	hashtags.push(hashtag);
			  	var tag = document.createElement('div');
			  	tag.className = 'tag';
			  	tag.innerHTML = hashtag+'<div class="tag-delete"></div>';
			  	this.parentNode.querySelector('.modal-bookmark__tags').appendChild(tag);
			  
			}	
			  
	  
			if(rec)
				sym != 8 ? hashtag += keyName : hashtag = hashtag.slice(0, hashtag.length-1);	  	
			}
						
			}, false);
	}

	///////////////////////////////////////////////////////////////submit bookmark
	submitBookmarkBtn.addEventListener('click', ()=>{
		
		var newBBody = document.querySelector('.bookmark-body_creation');
		var newBCaption = document.querySelector('.modal-bookmark_creation__title');
		var newbTags 		  =	document.querySelector('.modal-bookmark_creation__tags');

		var addNewBookmark = {id:quantity, title: newBCaption.value, body:newBBody.textContent, hashtags:hashtags};
		localStorage.setItem(quantity,JSON.stringify(addNewBookmark));
		
		

	

		render(shortcutsPlace, true, false, n=>{  //render one new bookmark
			n.addEventListener('click',()=>{
			recall(oldBookmark, n.dataset.id);
			show([oldBookmark,modalBg]);
			hide(newBookmark);
			
			},false);
			++quantity;
			localStorage.setItem('last',quantity);
			});
			newBBody.innerHTML = '';
			newBCaption.value = '';
			newbTags.innerHTML = '';
			hashtags = [];

			hide([newBookmark,modalBg]);

		});

	saveBtn.addEventListener('click', function(){
		var id  = this.parentNode.dataset.id;
		var existedHashtags  = JSON.parse(localStorage.getItem(id)).hashtags;
		var newBCaption = document.querySelector('.modal-bookmark__title').innerHTML;
		var newBBody = document.querySelector('.modal-bookmark__bookmark-body').textContent;

		hashtags =  existedHashtags.concat(hashtags);                         //TODO: get the func alright
		var updatedBookmark = {id:id, title:newBCaption, body:newBBody, hashtags:hashtags};

		toggleClass(saveBtn, 'disabled');
		localStorage.setItem(id,JSON.stringify(updatedBookmark));
		render(shortcutsPlace, true, id, n=>{
					n.addEventListener('click',()=>{
						recall(oldBookmark, n.dataset.id);
						show([oldBookmark,modalBg]);
						hide(newBookmark);
					},false);
			toggleContenteditable(editable);		
			removeClass(editable,'editing');
			alert('Bookmark #'+id+' was MEGAUPDATED');
			hide([oldBookmark,modalBg]);
		});

	});

	delBtn.addEventListener('click',function(){ //del bookmark
		var id = this.parentNode.dataset.id;
		if (confirm("Are you sure you want to DELETE Bookmark#"+id+"???")) {
			  deleteBookmark(id);
			  alert("Bookmark #"+id+" was MEGAREMOVED. Yes, this is your fault.");
			  hide([oldBookmark,modalBg]);
			} else {
			  console.log('ooof!');
			}

	});


}


///////////////////////////////////////////////////////////////who needs jQuery 
function hasClass(e, c){
	var classNames = e.className.split(' ');
	var res = false;


	classNames.forEach((e)=> {if(e == c) res = true;});
	return res;
}

function hide (elements) {
  elements = elements.length ? elements : [elements];
  for (var index = 0; index < elements.length; index++) {
    elements[index].style.display = 'none';
  }
}

function show (elements) {
  elements = elements.length ? elements : [elements];
  for (var index = 0; index < elements.length; index++) {
    elements[index].style.display = 'block';
  }
}

function toggleContenteditable(elements){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++){
	
		elements[i].contentEditable == 'true' ? elements[i].contentEditable = 'false' : elements[i].contentEditable = 'true';
	
	}
}
function removeClass(elements, c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
			//	elements[i].className.replace(new RegExp('(?:^|\\s)'+ c + '(?:\\s|$)'), ' '); TODO:fix regexp
				elements[i].classList.remove(c);
			}
}

function addClass(elements,c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
				elements[i].className += ' '+c;
			}
}

function toggleClass(elements, c){
	elements = elements.length ? elements : [elements];
	for(var i=0;i<elements.length;i++) {
				var res = hasClass(elements[i], c) ? removeClass(elements[i],c) : addClass(elements[i],c);
			}	
}


///////////////////////////////////////////////////////////////render function
function render(target, alone, updatedId, callback){
	
	var bmDull = document.createElement('div');
	bmDull.className = "bookmark-shortcut";
	bmDull.innerHTML = '<span class="bookmark-title"></span><div  class="bookmark-body bookmark-shortcut__body"></div>';
	
	
	if(!alone){  //all bookmarks
	
		for(i in localStorage) {	
			var cortege = JSON.parse(localStorage.getItem(i));
			if(cortege.id){
				var bm = bmDull.cloneNode(true);
				bm.dataset.id = cortege.id;
				bm.querySelector('.bookmark-title').innerHTML = cortege.title;
				bm.querySelector('.bookmark-body').innerHTML = cortege.body;
				target.appendChild(bm);
				callback(bm);
			}
		};	
		
	} else { //one bookmark
		
		if(!updatedId){ //new one bookmark
			var cortege = JSON.parse(localStorage.getItem(localStorage.getItem('last')));

			var bm = bmDull.cloneNode(true);
			bm.dataset.id = cortege.id;
			bm.querySelector('.bookmark-title').innerHTML = cortege.title;
			bm.querySelector('.bookmark-body').innerHTML = cortege.body;
			target.appendChild(bm);
			
			callback(bm);
			

		} else { //update old bookmark

			var victim = document.querySelector('[data-id="'+updatedId+'"]');
			var cortege = JSON.parse(localStorage.getItem(updatedId));
			var bm = bmDull.cloneNode(true);
			bm.dataset.id = cortege.id;
			bm.querySelector('.bookmark-title').innerHTML = cortege.title;
			bm.querySelector('.bookmark-body').innerHTML = cortege.body;
			
			target.replaceChild(bm, victim);
			callback(bm);
		}

	}

	console.log('rendered');
}


///////////////////////////////////////////////////////////////biew bookmark
function recall(elem, id){
	var cortege = JSON.parse(localStorage.getItem(id));
	var tagCon = elem.querySelector('.modal-bookmark__tags');
	

	tagCon.innerHTML = '';	

	elem.querySelector('.bookmark-title').innerHTML = cortege.title;
	elem.querySelector('.bookmark-body').innerHTML = cortege.body;
	elem.querySelector('.controls').dataset.id = cortege.id;

	appendTags(tagCon, cortege.hashtags, (tag)=>{  //render tags
		tagsTodel = [];
		
		var tagDel = tag.querySelector('.tag-delete');
		
		tagDel.addEventListener('mouseover', function(){
			addClass(this.parentNode, 'tag-victim');
		});

		tagDel.addEventListener('mouseout', function(){
			removeClass(this.parentNode, 'tag-victim');
		});

		tagDel.addEventListener('click', function(){
			var t = this.parentNode.textContent;
			hide(this.parentNode);
			removeClass(elem.querySelector('.modal-bookmark__sav'),'disabled');
			tagsTodel.push(t);
		});

		tag.addEventListener('mouseover',function(){
			show(this.querySelector('.tag-delete'));
		});

		tag.addEventListener('mouseout',function(){
			hide(this.querySelector('.tag-delete'));
		});

		tag.addEventListener('click',function(){
			hide(this.querySelector('.tag-delete'));
		});

		show(elem.querySelectorAll('.tag'));

	});

	colorTags(elem); //make hastags in the bookmark body color
}

///////////////////////////////////////////////////////////////color tags
function colorTags(e){

		e = e.querySelector('.bookmark-body'); 
	
		var inn = e.innerHTML.split(' ');
		e.innerHTML = '';
		for(var i =0; i <  inn.length; i++){
		
			if(inn[i].indexOf('#') == 0 ){
			
				inn[i] = ' <a href="#" class="tag-a">' +inn[i]+ '</a>'; 
				e.innerHTML += inn[i];
			} else 
				e.innerHTML += ' '+inn[i];

		} 
	}

///////////////////////////////////////////////////////////////remove bookmark
function deleteBookmark(id){
	var victim = document.querySelector('[data-id="'+id+'"]');
	localStorage.removeItem(id);
	victim.parentNode.removeChild(victim);
	
}

///////////////////////////////////////////////////////////////hadle kb input
function getChar(event) {

  if (event.which == null) { // IE
    if (event.keyCode < 32) return null; 
    return String.fromCharCode(event.keyCode)
  }

  if (event.which != 0 ) { // not IE
  	if(event.which == 8)
  		return event.which;
    if (event.which < 32) 
    	return null;  
    return String.fromCharCode(event.which); 
  }

  return null; 
}

///////////////////////////////////////////////////////////////append tags to bookmarks body
function appendTags(e, t, callback){
	var tagDull = document.createElement('div');
		tagDull.className = 'tag';
	for(var i = 0; i < t.length; i++){
		var tag = tagDull.cloneNode(true);
		tag.innerHTML = t[i]+'<div class="tag-delete"></div>';
		e.appendChild(tag);
		callback(tag);
	}
}


///////////////////////////////////////////////////////////////hadle removed hasthags TODO:fix it
function cleanHashtags(a,b){
    	for(var i = 0; i < a.length; i ++){
    		b.indexOf(a[i]) != -1 ? a.splice(i,1) : true;
    		console.log(a[i], '< hastag from clean func')	
    	}
    		


    	return a;
    }